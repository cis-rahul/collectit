<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['uses'=>'HomeController@index', 'as' => 'homepage']);
Route::get('signin', ['uses'=>'HomeController@index', 'as' => 'homepage']);
Route::post('signin', ['uses'=>'HomeController@signin', 'as' => 'signin']);
Route::get('dashboard', ['uses'=>'HomeController@dashboard', 'as' => 'dashboard']);
Route::post('dashboard', ['uses'=>'HomeController@dashboard', 'as' => 'dashboard']);
Route::get('signup', ['uses'=>'HomeController@signup', 'as' => 'signup']);
Route::post('signup', ['uses'=>'HomeController@dosignup', 'as' => 'dosignup']);
Route::get('getdata', ['uses'=>'HomeController@getData', 'as' => 'getdata']);
//Route::resource('table','ClientTableManagementController');