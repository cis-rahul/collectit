<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{  
    public function __construct() {
        //parent::__construct(); 
        //die("Hi");
    }
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
