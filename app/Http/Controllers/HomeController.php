<?php

namespace App\Http\Controllers;
use App\Http\Controllers;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistrationRequest;
use Illuminate\Support\Facades\Redirect;
use View;
use App\User;
use Session;
use Auth;
use Validator;





//use Input;


class HomeController extends AuthController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        
    }
        
    
    public function index(){        
        return View::make('login');
    }
    
    public function signup(){               
        return View::make('registration');
    }
    
    public function dosignup(RegistrationRequest $request){ 
        $userinfo = User::select('mail')->where('mail', '=', $request->input('email'))->first();
        $flash = "";
        if(empty($userinfo)){
            if($this->create($request->all())){
                $flash = array('flash_alert_notice'=> 'User Registration done successfully !', 'flash_action'=>'success');
            }else{
                $flash = array('flash_alert_notice'=> 'Somthing went wrong!', 'flash_action'=>'danger');
            }
        }else{
            $flash =  array('flash_alert_notice'=> 'This user already exist !', 'flash_action'=>'success');
        }
        return Redirect::to('signin')->with($flash)->withInput();
    }

    public function dashboard(){    
        return View::make('front.dashboard');
    }

    public function test(){
        return View::make('blank');
    }
    public function signin(LoginRequest $request){        
        $credentials = array('email' => $request->input('email'), 'password' => $request->input('password'));
        if (Auth::attempt($credentials)) {
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('signin')->with(array('flash_alert_notice'=> 'Wrong Email or Password. Try again!', 'flash_action'=>'danger'))->withInput();
        }        
    }

    public function getData(){
          $team_data  = Business::select('*')->get()->toArray();
          foreach($team_data as $objTeam){
                echo "<pre>";
                print_r($objTeam);die;
          }
          return $team_data;
          echo json_encode(array("result"=>$team_data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */    
}
