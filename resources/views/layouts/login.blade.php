<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>:: collectit ::</title>        
        {!! HTML::style('https://fonts.googleapis.com/css?family=Lato:400,100italic,100,300,300italic,400italic,700,700italic,900,900italic') !!}
        {!! HTML::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css') !!}
        {!! HTML::style('public/css/bootstrap.min.css') !!}
        {!! HTML::style('public/css/style.css') !!}
        {!! HTML::style('public/css/responsive.css') !!}
    </head>
    <body>
        <!--wrapper start-->
        <div class="wraaper">
            <!--login top start-->
            <div class="login-top">
                <div class="container">
                    <ul>
                        <li><a href="login.html"><span><i class="fa fa-lock" aria-hidden="true"></i></span>Login</a></li>
                        <li><a href="register.html"><span><i class="fa fa-key" aria-hidden="true"></i></span>Register</a></li>
                    </ul>
                </div>
            </div>
            <!--header start-->
            <div class="header">
                <div class="container">
                    <div class="logo-box">
                        <a href="javascript:void(0)"><img src="public/images/logo.png" alt=""></a>
                    </div>
                </div>
            </div>
            @yield('content')            
        </div>
        <!--wrapper end-->
        <script src="http://code.jquery.com/jquery-1.12.0.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </body>
</html>