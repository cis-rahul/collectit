<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login | Collectit</title>
    <!--global css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/bootstrap.min.css') }}">
    <link rel="shortcut icon" href="{{ asset('public/assets/images/favicon.png') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('public/assets/images/favicon.png') }}" type="image/x-icon">
    <!--end of global css-->
    <!--page level css starts-->
    <link type="text/css" rel="stylesheet" href="{{asset('public/assets/vendors/iCheck/skins/all.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/frontend/login.css') }}">
    <!--end of page level css-->
</head>
<body>
<div class="container">
    <!--Content Section Start -->
    <div class="row">
        <div class="box animation flipInX">
            <div class="box1">
            <img src="{{ asset('public/assets/images/josh-new.png') }}" alt="logo" class="img-responsive mar">
            <h3 class="text-primary">Login</h3>
                <!-- Notifications -->                
                @if (session('flash_alert_notice'))
                    <div class="flash-message">
                        <div class="alert alert-{{session('flash_action')}}">
                               {{session('flash_alert_notice')}}
                        </div>
                    </div>
                @endif
                <form action="{{route('signin')}}" class="omb_loginForm"  autocomplete="off" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group {{ $errors->first('email', 'has-error') }}">
                        <label class="sr-only">Email</label>
                        <input type="email" class="form-control" name="email" placeholder="Email" value="{!! Input::old('email') !!}">
                    </div>
                    <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                    <div class="form-group {{ $errors->first('password', 'has-error') }}">
                        <label class="sr-only">Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <span class="help-block">{{ $errors->first('password', ':message') }}</span>
                    <div class="checkbox col-sm-12">
<!--                        <label>
                            <input type="checkbox">  Remember Password
                        </label>-->
                        <label>
                        {!! Form::submit('Facebook', array('class' => 'facebook-button button-social-login-facebook')) !!}
                        </label>
                    </div>
                    <input type="submit" class="btn btn-block btn-primary" value="Login">                    
                    Don't have an account? <a href="{{route('signup')}}"><strong> Sign up</strong></a>
                </form>
            </div>
        <div class="bg-light animation flipInX">
            <a href="#">Forgot Password?</a>
        </div>
        </div>
    </div>
    <!-- //Content Section End -->
</div>
<!--global js starts-->
<script type="text/javascript" src="{{ asset('public/assets/js/frontend/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/frontend/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/iCheck/icheck.min.js') }}"></script>
<!--global js end-->
<script>
    $(document).ready(function(){
        $("input[type='checkbox']").iCheck({
            checkboxClass: 'icheckbox_minimal-blue'
        });
    });

//Load the Facebook JS SDK
    (function(d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "http://connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));

    // Init the SDK upon load
    window.fbAsyncInit = function() {
        FB.init({
            appId: '213702215678816', // App ID
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true, // parse XFBML
        });


        // Specify the extended permissions needed to view user data
        // The user will be asked to grant these permissions to the app (so only pick those that are needed)
        var permissions = [
            'email',
            //        'user_likes',
            //        'friends_likes',
            'user_about_me',
                    //        'friends_about_me',
                    //        'user_birthday',
                    //        'friends_birthday',
                    //        'user_education_history',
                    //        'friends_education_history',
                    //        'user_hometown',
                    //        'friends_hometown',
                    //        'user_relationships',
                    //        'friends_relationships',
                    //        'user_relationship_details',
                    //        'friends_relationship_details',
                    //        'user_location',
                    //        'friends_location',
                    //        'user_religion_politics',
                    //        'friends_religion_politics',
                    //        'user_website',
                    //        'friends_website',
                    //        'user_work_history',
                    //        'friends_work_history'
        ].join(',');

        // Specify the user fields to query the OpenGraph for.
        // Some values are dependent on the user granting certain permissions
        var fields = [
            'id',
            'name',
            'first_name',
            'middle_name',
            'last_name',
            'gender',
            //'locale',
            //'languages',
            'link',
            //        'third_party_id',
            //        'installed',
            //        'timezone',
            //        'updated_time',
            //        'verified',
            //        'age_range',
            'bio',
            //        'birthday',
            //        'cover',
            //        'currency',
            //        'devices',
            //        'education',
            'email',
                    //        'hometown',
                    //        'interested_in',
                    //        'location',
                    //        'political',
                    //        'payment_pricepoints',
                    //        'favorite_athletes',
                    //        'favorite_teams',
                    //        'picture',
                    //        'quotes',
                    //        'relationship_status',
                    //        'religion',
                    //        'significant_other',
                    //        'video_upload_limits',
                    //        'website',
                    //        'work'
        ].join(',');

        function showDetails() {
            FB.api('/me', {fields: fields}, function(details) {
                var base_path = "{{route('homepage')}}";
                var fb_url = base_path + "/test";
                alert(fb_url);
                details['_token'] = "SOzQlGn9G70x4MUkMYR4xHI8MxlLwUiiMIdIMukV";
                alert(details['email']);
                $.ajax({
                    url: fb_url,
                    asys: false,
                    data: details,
                    type: "GET",
                    success: function(data) {
                        window.location.href = fb_url;
                    }
                });
                $('#fb-login').attr('style', 'display:none;');
            });
        }


        //$('.button-social-login-facebook').click(function () {
        $(document).on('click', '.button-social-login-facebook', function() {
            //initiate OAuth Login
            FB.login(function(response) {
                // if login was successful, execute the following code
                if (response.authResponse) {
                    showDetails();
                }
            }, {scope: permissions});
        });

    };
</script>
</body>
</html>
