<div class="side-menu">
    <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">
                    <div class="icon fa fa-paper-plane"></div>
                    <div class="title">Flat Admin V.2</div>
                </a>
                <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                    <i class="fa fa-times icon"></i>
                </button>
            </div>
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{ route("admin.dashbaord") }}">
                        <span class="icon fa fa-tachometer"></span><span class="title">Dashboard</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route("admin.allsession") }}">
                        <span class="icon fa fa-desktop"></span><span class="title">Season</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route("admin.allteams") }}">
                        <span class="icon fa fa-cubes"></span><span class="title">Teams</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route("admin.allplayers") }}">
                        <span class="icon fa fa-user"></span><span class="title">Players</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route("admin.allmatches") }}">
                        <span class="icon fa fa-user"></span><span class="title">Matchs</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route("admin.allump") }}">
                        <span class="icon fa fa-cubes"></span><span class="title">Umpires</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
</div>