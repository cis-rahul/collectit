<head>

    <title>Josh Admin Template</title>
    <meta charset="UTF-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="public/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> {!! HTML::style('public/assets/css/bootstrap.min.css') !!} {!! HTML::style('public/assets/vendors/font-awesome/css/font-awesome.min.css') !!} {!! HTML::style('public/assets/css/styles/black.css') !!} {!! HTML::style('public/assets/css/panel.css') !!} {!! HTML::style('public/assets/css/metisMenu.css') !!} {!! HTML::style('public/assets/vendors/fullcalendar/css/fullcalendar.css') !!} {!! HTML::style('public/assets/css/pages/calendar_custom.css') !!} {!! HTML::style('public/assets/vendors/jvectormap/jquery-jvectormap.css') !!} {!! HTML::style('public/assets/vendors/animate/animate.min.css') !!} {!! HTML::style('public/assets/css/only_dashboard.css') !!}
    <!--end of page level css-->
</head>